# NetworkingAPI

Networking API is a URLSessionDataTask abstraction layer

## Installation
---------------------------------------------------------------------------------
Networking API is available through Swift Package Manager

Add this URL to File -> Swift Packages -> Add new Dependency
```
https://gitlab.com/justin126/networkingapi
```
---------------------------------------------------------------------------------
## Usage
import NetworkingAPI

```
import NetworkingAPI
```

Declare an enum representing your API endpoints

```
enum APIEndpoint {
    case login(emailId: String, password: String)
    case getProfile
    case updateProfile
    case changeProfileImage(profileImage: UIImage)
}
```

Create extension of enum and conform to the API protocol

```
extension APIEndpoint: API {

var baseURL: URL {
    URL(string: "<base url>")
  }
}
```

```
// endpoint path
var path: String {
    switch self {
    case login:
        return "login"
    case getProfile:
        return "get_profile"
    case updateProfile:
        return "update_profile"
    case changeProfileImage:
        return "change_profile_image"
  }
}
```

```
// HTTP method of endpoint
var method: ElegantAPI.Method {
    switch self {
    case .login:
        return .post
    case .getProfile:
        return .get
    case .updateProfile:
        return .patch
    case changeProfileImage:
        return .put
    }

var sampleData: Data {
    // for mocking requests
    Data()
}

var task: Task {
    switch self {
    
    case .login(let email,let password):
        return .requestParameters(parameters: [
            "email": email,
            "password": password
        ], encoding: .JSONEncoded)
        
    case .getProfile:
        return .requestPlain
        
    case .updateProfile(let user):
        return .requestJSONEncodable(user)
        
    case .changeProfileImage(let image):
        var data: [MultipartFormData] = [
            MultipartFormData(
                data: image.jpegData(compressionQuality: 0.4) ?? Data(),
                name: "image",
                fileName: "\(UUID().uuidString).png",
                mimeType: "image/jpeg"
            )
        ]
        return .uploadMultipart(data)
        
    }
}

// Headers for request
var headers: [String : String]? {

    switch self {
    case .login:
        return ["Content-Type": "application/json"]
    case .getProfile, .updateProfile, .changeProfileImage:
        return ["Authorization": "Bearer \(Persistance.shared.accessKey)"]
    }
}
```
