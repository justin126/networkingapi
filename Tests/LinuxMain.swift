import XCTest

import NetworkingAPITests

var tests = [XCTestCaseEntry]()
tests += NetworkingAPITests.allTests()
XCTMain(tests)
