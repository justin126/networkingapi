//
//  Encodable.swift
//  
//  Created by Justin Trautman on 8/11/20.
//

import Foundation

extension Encodable {
    public func toJSONData() throws -> Data { try JSONEncoder().encode(self) }
    public func toJSONData(_ encoder: JSONEncoder) throws -> Data { try encoder.encode(self) }
}
