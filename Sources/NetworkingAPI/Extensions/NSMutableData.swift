//
//  NSMutableData.swift
//  
//  Created by Justin Trautman on 8/11/20.
//

import Foundation

extension NSMutableData {
    public func appendString(_ string: String) {
        if let data = string.data(using: .utf8) {
            self.append(data)
        }
    }
}
