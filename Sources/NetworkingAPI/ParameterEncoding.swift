//
//  ParameterEncoding.swift
//  
//  Created by Justin Trautman on 8/11/20.
//

import Foundation

public enum ParameterEncoding {
    case URLEncoded
    case JSONEncoded
}
